using System.ComponentModel.DataAnnotations;

namespace MobileScooter.Api.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "RequiredPassword")]
        public string Password { get; set; }
        [Required(ErrorMessage = "RequiredEmail")]
        public string Email { get; set; }
    }
}