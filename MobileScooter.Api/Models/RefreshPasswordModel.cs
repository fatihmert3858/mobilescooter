using System.ComponentModel.DataAnnotations;

namespace MobileScooter.Api.Models
{
    public class RefreshPasswordModel
    {
        [Required(ErrorMessage = "RequiredPassword")]
        public string CurrentPassword { get; set; }
        [Required(ErrorMessage = "RequiredPassword")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "RequiredEmail")]
        public string Email { get; set; }
    }
}