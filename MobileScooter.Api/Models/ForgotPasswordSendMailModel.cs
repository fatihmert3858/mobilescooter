using System.ComponentModel.DataAnnotations;

namespace MobileScooter.Api.Models
{
    public class ForgotPasswordSendMailModel
    {
        public string ToEmail { get; set; }
        public string ToName { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        
    }
}