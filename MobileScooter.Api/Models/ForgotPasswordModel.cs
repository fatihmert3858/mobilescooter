using System.ComponentModel.DataAnnotations;

namespace MobileScooter.Api.Models
{
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage ="RequiredEmail")]
        public string Email { get; set; }
        [Required(ErrorMessage = "RequiredName")]
        public string Name { get; set; }
        [Required(ErrorMessage = "RequiredIdentity")]
        public string Identity { get; set; }
    }
}