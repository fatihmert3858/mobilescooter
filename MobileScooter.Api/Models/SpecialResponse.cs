namespace MobileScooter.Api.Models
{
    public class SpecialResponse<T>
    {
        public string ErrorName { get; set; }
        public bool Error { get; set; }
        public T Data { get; set; }
        public string SuccessMessage { get; set; }
    }
}