using Microsoft.AspNetCore.Mvc;

namespace MobileScooter.Api.Models
{
    public class ValidatorResponse: ObjectResult
    {
        public ValidatorResponse(ApiResponse<object> response) : base(response)
        {
            
        }
    }
}