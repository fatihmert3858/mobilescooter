using System.ComponentModel.DataAnnotations;

namespace MobileScooter.Api.Models
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "RequiredEmail")]
        public string Email { get; set; }
        [Required(ErrorMessage = "RequiredPassword")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "RequiredIdentity")]
        public string Identity { get; set; }
        [Required(ErrorMessage = "RequiredVerifCode")]
        public string VerificationCode { get; set; }
    }
}