using System.ComponentModel.DataAnnotations;
using MobileScooter.Api.Data.Entities;

namespace MobileScooter.Api.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage ="RequiredName")]
        public string Name { get; set; }
        [Required(ErrorMessage = "RequiredSurname")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "RequiredEmail")]
        public string Email { get; set; }
        [Required(ErrorMessage = "RequiredPassword")]
        public string Password { get; set; }
        [Required(ErrorMessage = "RequiredPhone")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "RequiredIdentity")]
        public string Identity { get; set; }
        [Required(ErrorMessage = "RequiredFaculty")]
        public string Faculty { get; set; }
        [Required(ErrorMessage = "RequiredDepartment")]
        public string Department { get; set; }
        [Required(ErrorMessage = "RequiredGender")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "RequiredRole")]
        public string Role { get; set; }
    }
}