using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Email
{
    public class SmtpEmailService : IEmailService
    {
        public async Task SendMail(ForgotPasswordSendMailModel model)
        {
            var mimeMessage = new MimeMessage();
             // Authentication Hatası OLur ise, google accounts security Less Secure App Access Enable olmalı
            mimeMessage.From.Add(new MailboxAddress("Mobile Scooter","erumobilescooter@gmail.com"));
            mimeMessage.To.Add(new MailboxAddress(model.ToName, model.ToEmail));
            mimeMessage.Subject = model.Subject;
            mimeMessage.Body = new TextPart("plain"){
                Text = $"<strong>{model.Message}</strong>"
            };
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com",587,false);
                await client.AuthenticateAsync("erumobilescooter@gmail.com","Mert_123@@-");
                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);
            }

        }
    }
}