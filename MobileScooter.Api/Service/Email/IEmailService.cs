using System.Threading.Tasks;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Email
{
    public interface IEmailService
    {
        Task SendMail(ForgotPasswordSendMailModel model);
    }
}