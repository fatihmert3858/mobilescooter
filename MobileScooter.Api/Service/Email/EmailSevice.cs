using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Net;
using System;
using System.Collections.Generic;
using SendGrid;
using SendGrid.Helpers.Mail;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Email
{
    public class SendGridEmailSevice : IEmailService
    {

        private readonly IConfiguration configuration;

        public SendGridEmailSevice(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public Task SendMail(ForgotPasswordSendMailModel model)
        {
            var apiKey = configuration.GetSection("SendGridApiKey").Value;
            return Execute(apiKey,model.Subject,model.Message, model.ToEmail, model.ToName);
        }

        public async Task Execute(string apiKey, string subject, string message, string email, string toName)
        {
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("mobilescooter@eru.com", "Fatih Rahman Mert");
            var to = new EmailAddress(email, toName);
            var plainTextContent = message;
            var htmlContent = $"<strong>{message}</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }

    }
}