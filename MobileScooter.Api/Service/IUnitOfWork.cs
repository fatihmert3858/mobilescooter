using System.Threading.Tasks;
using MobileScooter.Api.Data;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service.Email;
using MobileScooter.Api.Service.Repositories.Abstract;

namespace MobileScooter.Api.Service
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository {get;}
        IAccountRepository AccountRepository {get;}
        IVerificationPasswordRepository VerificationPasswordRepository {get;}
        IScooterRepository ScooterRepository {get;}

        IEmailService EmailService {get;}
        ApplicationContext Context {get;}
        ILanguage Language {get;}
        void Commit();
        Task CommitAsync();

        void Rollback();
    }
}