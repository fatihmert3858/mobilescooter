using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EruMobileScooter.Data.Entities;
using Microsoft.EntityFrameworkCore;
using MobileScooter.Api.Data;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service.Repositories.Abstract;

namespace MobileScooter.Api.Service.Repositories.Concreate
{
 public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity 
    {

        private readonly ApplicationContext _context;
        private readonly ILanguage language;

        public BaseRepository(ApplicationContext context, ILanguage language)
        {
            _context = context;
            this.language = language;
        }

        /**
        *   Marks As Deleted, Given Entity
        *   param name="entity" must be checed null or empty property
        *   message Deleted means, The Data Marked Deleted, Process Successfull
        *   message Failled means, The Data No Marked Deleted, Process No Success
        */
        public async Task<bool> DeleteAsync(T entity)
        {
            var result = await _context.Set<T>().FindAsync(entity.Id);
            if(result == null)
                return false;
            var deletedEntry = _context.Remove<T>(result);
            return _context.Entry<T>(deletedEntry.Entity).State == EntityState.Deleted;
        }
        /**
        *   Marks As Deleted, Given Entity Id
        *   param name="id" must be checked null or empty
        *   message NotFound means, The Data Not Found In Database
        *   message Failled means, The Data No Marked Deleted
        *   message Deleted means, The Data Marks Deleted
        */
        public async Task<bool> DeleteAsync(string id)
        {
            var entity = await _context.FindAsync<T>(id);
            if(entity == null)
                return false;
            
            var deletedEntity = _context.Remove<T>(entity);
            return  _context.Entry<T>(deletedEntity.Entity).State == EntityState.Deleted;
        }
        /**
        *   Gets Entity given id.
        *   param name="id" must be checked null or empty
        *   message Not Found means, The id Not Found In Database
        *   message Success means, The Data got successed
        */
        public async Task<T> GetAsync(string id)
        {
            return await _context.FindAsync<T>(id);
        }
        /**
        *   Get all data in database
        *   message ListEmptyOrNull means, The Database objects is null or empty 
        *   message Success means, The Datas Gots Successed
        */
        public async Task<IList<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        /**
        *   Inserting Database for given entity
        *   param name="entity" must be checked null or empty property
        *   message Created means, The Data Inserted Database
        *   message Failled means, The Data No Inserted Database
        *   
        */
        public async Task<T> InsertAsync(T entity)
        {
            var addedEntity = await _context.AddAsync<T>(entity);
            if(_context.Entry<T>(addedEntity.Entity).State == EntityState.Added)
              return addedEntity.Entity;
            return null;
        }

        /**
        *   Update Entity In Database Given Entity Instance
        *   param name="entity" must be checked null or empty property
        *   message Updated means, The Data Successfuly Updated
        *   message Failled means, The Data Failled on Updating 
        */
        public T Update(T entity)
        {
            var updatedEntity =  _context.Set<T>().Update(entity);
            if(updatedEntity.State == EntityState.Modified)
                return updatedEntity.Entity;
            return null;
        }
    }
}