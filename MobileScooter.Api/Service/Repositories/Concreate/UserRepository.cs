using System;
using System.Linq;
using System.Threading.Tasks;
using MobileScooter.Api.Data;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service.Repositories.Abstract;

namespace MobileScooter.Api.Service.Repositories.Concreate
{
 public class UserRepository :  BaseRepository<User>,IUserRepository
    {
        private readonly ApplicationContext _context;
        private readonly ILanguage language;
        public UserRepository(ApplicationContext context,ILanguage language) : base(context,language)
        {
            if (context == null)
                throw new ArgumentNullException("ApplicationContext Instance Can Not Be Null");
            else
                _context = context;
            
            this.language = language;
        }

        public User GetByEmail(string email)
        {
            if(email == null || email == "")
               return null;
            return _context.Users.Where(x => x.Email == email).FirstOrDefault();
        }

        /**
        * Verifying user object and userId.
        */
        public async Task<bool> Verify(string id, User user)
        {
            if(user == null)
                return false;
            var remoteUser = await GetAsync(id);
            if(remoteUser != null)
                return user.Equals(remoteUser);
            return false;
        }
    }
}