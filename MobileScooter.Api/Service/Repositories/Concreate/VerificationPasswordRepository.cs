using System.Linq;
using MobileScooter.Api.Data;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service.Repositories.Abstract;

namespace MobileScooter.Api.Service.Repositories.Concreate
{
    public class VerificationPasswordRepository: BaseRepository<VerificationPassword>, IVerificationPasswordRepository
    {
        private readonly ApplicationContext context;
        private readonly ILanguage language;
        public VerificationPasswordRepository(ApplicationContext context, ILanguage language): base(context,language)
        {
            this.context = context;
            this.language = language;
        }

        public VerificationPassword GetByUserEmail(string userEmail)
        {
            if(userEmail == "" || userEmail == null)
                return null;
            
            return context.VerificationPasswords.Where(x => x.UserEmail == userEmail).FirstOrDefault();
        }
    }
}