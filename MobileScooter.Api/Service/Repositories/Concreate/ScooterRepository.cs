using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MobileScooter.Api.Data;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Exceptions;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Service.Repositories.Abstract;

namespace MobileScooter.Api.Service.Repositories.Concreate
{
    public class ScooterRepository :  BaseRepository<Scooter>,IScooterRepository
    {
        private readonly ApplicationContext context;
        private readonly ILanguage language;
        public ScooterRepository(ApplicationContext context,ILanguage language) : base(context,language)
        {
            this.context = context;
            this.language = language;
        }

        public async Task<string> GetBarcode(string id)
        {
            if(string.IsNullOrEmpty(id))
                throw new ResponseException(language.WrongInput);
            var scooter  = await context.Scooters.FindAsync(id);
            if(scooter != null)
                return scooter.Barcode;
            return null;
        }

        public async Task<string> GetBarcode(int number)
        {
            if(number < 0)
                throw new ResponseException(language.WrongInput);
            var scooter = await context.Scooters.FirstOrDefaultAsync(x => x.Number == number);
            if(scooter != null)
                return scooter.Barcode;
            return null;
        }

        public async Task<Scooter> GetByNumber(int number)
        {
            if(number < 0)
                throw new ResponseException(language.WrongInput);
            
            return await context.Scooters.FirstOrDefaultAsync(x => x.Number == number);
        }

        public async Task<int> GetChargeState(int number)
        {
            if(number < 0)
                throw new ResponseException(language.WrongInput);
            var scooter = await context.Scooters.FirstOrDefaultAsync(x => x.Number == number);
            if(scooter != null)
                return scooter.ChargeState;
            return -1;
            
        }

        public async Task<int> GetChargeState(string id)
        {
            if(string.IsNullOrEmpty(id))
                throw new ResponseException(language.WrongInput);
            var scooter = await context.Scooters.FindAsync(id);
            if(scooter != null)
                return scooter.ChargeState;
            return -1;
        }

        public async Task<int> GetCount()
        {
            var list = await context.Scooters.ToListAsync();
            return list.Count;
        }

        public async Task<string> GetLocation(string id)
        {
            if(string.IsNullOrEmpty(id))
                throw new ResponseException(language.WrongInput);
            var scooter = await context.Scooters.FindAsync(id);
            if(scooter != null)
                return scooter.Location;
            return null;
        }

        public async Task<string> GetLocation(int number)
        {
            if(number < 0)
                throw new ResponseException(language.WrongInput);
            var scooter = await context.Scooters.FirstOrDefaultAsync(x => x.Number == number);
            if(scooter != null)
                return scooter.Location;
            return null;
        }
    }
}