using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MobileScooter.Api.Data;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Exceptions;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service.Email;
using MobileScooter.Api.Service.Repositories.Abstract;
using MobileScooter.Api.Utils;

namespace MobileScooter.Api.Service.Repositories.Concreate
{
    public class AccountRepository : IAccountRepository
    {

        private readonly ApplicationContext context;
        private readonly ILanguage language;
        private readonly IEmailService emailService;

        public AccountRepository(ApplicationContext context, ILanguage language, IEmailService emailService)
        {
            this.language = language;
            this.context = context;
            this.emailService = emailService;
        }

        /**
        *   param name="model" must be checked null or empty property
        */
        public async Task<string> Login(LoginModel model)
        {
            // Kullanıcı var mı?, token donder
            var user = await context.Users.Where(x => x.Email == model.Email).FirstOrDefaultAsync();
            
            if(user == null)
                throw new ResponseException(language.UserNotFound);
            
            var correctPassword = UserHelper.IsValidPassword(model.Password,user.PasswordHash);
            if(!correctPassword)
                throw new ResponseException(language.WrongPassword);
            
            var token = await TokenUtil.CreateTokenAsync(user.Id,user.Name,user.Role);
            return token;
        }

        public async Task<User> RefreshPassword(RefreshPasswordModel model)
        {
            var user = await context.Users.Where(x => x.Email == model.Email).FirstOrDefaultAsync();
            if(user == null)
                throw new ResponseException(language.UserNotFound);

            var isCorrectPassword = UserHelper.IsValidPassword(model.CurrentPassword,user.PasswordHash);
            
            if(!isCorrectPassword)
                throw new ResponseException(language.WrongPassword);

            var newPasswordHash = UserHelper.CreatePasswordHash(model.NewPassword);
            user.PasswordHash = newPasswordHash;
            return user;
        }

        /**
        *   param name="model" must be checked null or empty property
        */
        public async Task<User> Register(RegisterModel model)
        {
            var passwordHash = UserHelper.CreatePasswordHash(model.Password);
            var user = new User{Department=model.Department,Email=model.Email,Faculty=model.Faculty,Gender=model.Gender,Identity=model.Identity,
                                Role=model.Role,Name=model.Name,PasswordHash=passwordHash,Phone=model.Phone,Surname=model.Surname};

            var result = await context.Users.AddAsync(user);

            if(!(result.State == EntityState.Added))
                throw new ResponseException(language.Failled);

             return user;
        }

         public async Task<string> ForgotPasswordSendEmail(ForgotPasswordModel model){
            var random = new Random();
            var verificationKey = random.Next(0,999999).ToString();
            var forgotPasswordModel = new ForgotPasswordSendMailModel{ToName=model.Name, ToEmail=model.Email, Message=$"Verification key is {verificationKey}",
                                        Subject="Forgot Password"};
            await emailService.SendMail(forgotPasswordModel);
            return verificationKey;
         }

        public async Task<User> ResetPassword(ResetPasswordModel model)
        {
            var user = await context.Users.Where(x => x.Email == model.Email && x.Identity == model.Identity).FirstOrDefaultAsync();
            if(user == null)
                throw new ResponseException(language.UserNotFound);
            
            var newPasswordHash = UserHelper.CreatePasswordHash(model.NewPassword);
            user.PasswordHash = newPasswordHash;
            return user;
        }
    }
}