using System.Threading.Tasks;
using MobileScooter.Api.Data.Entities;

namespace MobileScooter.Api.Service.Repositories.Abstract
{
    public interface IScooterRepository : IBaseRepository<Scooter>
    {
        Task<Scooter> GetByNumber(int number);
        Task<int> GetCount();
        Task<string> GetLocation(string id);
        Task<string> GetLocation(int number);
        Task<int> GetChargeState(int number);
        Task<int> GetChargeState(string id);
        Task<string> GetBarcode(string id);
        Task<string> GetBarcode(int number);
    }
}