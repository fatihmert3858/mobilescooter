using System.Collections.Generic;
using System.Threading.Tasks;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Repositories.Abstract
{
    public interface IUserRepository : IBaseRepository<User>
    {
        /**
        * Verilen Id ile see user ın aynı olup olmadıgı
        */
        Task<bool> Verify(string id, User user);

        User GetByEmail(string email);
    }
}