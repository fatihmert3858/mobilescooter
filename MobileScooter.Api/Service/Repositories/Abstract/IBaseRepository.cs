using System.Collections.Generic;
using System.Threading.Tasks;
using EruMobileScooter.Data.Entities;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Repositories.Abstract
{
    public interface IBaseRepository<T>  where T : BaseEntity
    {
        Task<T> GetAsync(string id);
        Task<IList<T>> GetAllAsync();
        Task<T> InsertAsync(T entity);
        Task<bool> DeleteAsync(T entity);
        Task<bool> DeleteAsync(string id);
        T Update(T entity);
    }
}