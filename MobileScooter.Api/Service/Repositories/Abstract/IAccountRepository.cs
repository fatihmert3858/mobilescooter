using System.Threading.Tasks;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Repositories.Abstract
{
    public interface IAccountRepository
    {
         
        Task<string> Login(LoginModel model);

        Task<User> Register(RegisterModel model);

        Task<User> RefreshPassword(RefreshPasswordModel model);

        Task<User> ResetPassword(ResetPasswordModel model);
        Task<string> ForgotPasswordSendEmail(ForgotPasswordModel model);
    }
}