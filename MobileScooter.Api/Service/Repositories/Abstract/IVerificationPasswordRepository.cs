using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Service.Repositories.Abstract
{
    public interface IVerificationPasswordRepository : IBaseRepository<VerificationPassword>
    {
        VerificationPassword GetByUserEmail(string userEmail);
    }
}