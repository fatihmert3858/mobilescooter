using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MobileScooter.Api.Data;
using MobileScooter.Api.Exceptions;
using MobileScooter.Api.Handler;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service.Email;
using MobileScooter.Api.Service.Repositories.Abstract;
using MobileScooter.Api.Service.Repositories.Concreate;

namespace MobileScooter.Api.Service
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(ApplicationContext context, ILanguage language, IEmailService emailService)
        {
            Context = context;
            Language = language;
            EmailService = emailService;
        }
        public ApplicationContext Context {get;}
        public ILanguage Language {get;}

        public IEmailService EmailService {get;}

        private AccountRepository _accountRepository;
        public IAccountRepository AccountRepository{
            get{
                if(_accountRepository == null)
                    _accountRepository = new AccountRepository(Context,Language,EmailService);
                return _accountRepository;
            }
        }

        private UserRepository _userRepository;
        public IUserRepository UserRepository {
            get {
                if(_userRepository == null)
                    _userRepository = new UserRepository(Context,Language);
                return _userRepository;
            }
        }

        private VerificationPasswordRepository _verificationPasswordRepository;
        public IVerificationPasswordRepository VerificationPasswordRepository {
            get {
                if(_verificationPasswordRepository == null)
                    _verificationPasswordRepository = new VerificationPasswordRepository(Context,Language);
                return _verificationPasswordRepository;
            }
        }

        private ScooterRepository _scooterRepository;
        public IScooterRepository ScooterRepository {
            get {
                if(_scooterRepository == null)
                    _scooterRepository = new ScooterRepository(Context,Language);  
                return _scooterRepository;
            }
        }

        public void Commit()
        {
           using(var transaction = Context.Database.BeginTransaction()){
               try{
                    Context.SaveChanges();
                    transaction.Commit(); 
               }catch (System.Exception)
               {
                   transaction.Rollback();
                   throw new ResponseException("Commit Inside");
               }
           }
        }

        public async Task CommitAsync()
        {
            using(var transaction = await Context.Database.BeginTransactionAsync())
            {
               try{
                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync(); 
               }catch (System.Exception ex)
               {
                    await transaction.RollbackAsync();
                    var cause = ApplicationContextHandler.CommitHandle(ex);
                    var isValid = cause.Keys.Contains("UserEx");
                    var message = "";
                    if(isValid)
                       message = cause["UserEx"];
                    throw new ResponseException(message);
               }
           }
        }

        public void Rollback()
        {
           Context.Database.RollbackTransaction();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}