
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service;

namespace MobileScooter.Api.Controllers
{
    [Authorize(Roles= "ADMIN,TECHNICIAN")]
    [ApiController]
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILanguage language;

        public UserController(IUnitOfWork unitOfWork, ILanguage language){
            _unitOfWork = unitOfWork;
            this.language = language;
        }
        /**
        *   Returns User List
        */
        [HttpGet("all")]
        public async Task<ApiResponse<IList<User>>> GetUsers(){
            
            var result = await _unitOfWork.UserRepository.GetAllAsync();
            IList<User> users = new List<User>();
            foreach (var user in result)
                if(!user.isBanned) users.Add(user);

            return ResponseHelper.CreateApiResponse<IList<User>>(users,language.Success,1,false);
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse<User>> GetUser(string id){
            if (id.Equals("") || id == null)
                return ResponseHelper.CreateApiResponse<User>(null, language.WrongInput, 2, true);
            var user = await _unitOfWork.UserRepository.GetAsync(id);
            return ResponseHelper.CreateApiResponse<User>(user,language.Success,1,false);
        }

        [Authorize("ADMIN")]
        [HttpPut("update")]
        public async Task<ApiResponse<User>> Update([FromBody] User user){
            
            var result = _unitOfWork.UserRepository.Update(user);
            if(result == null)
                return ResponseHelper.CreateApiResponse<User>(null,language.Failled,2,true);
            await _unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse<User>(result,language.Updated,1,false);
        }

        [Authorize("ADMIN")]
        [HttpDelete("delete/{id}")]
        public async Task<ApiResponse<bool>> Delete(string id){
            if(id == null || id.Equals(""))
                return ResponseHelper.CreateApiResponse<bool>(false,language.WrongInput,2,true);

            var deleteResult = await _unitOfWork.UserRepository.DeleteAsync(id);
            if(deleteResult == false)
                return ResponseHelper.CreateApiResponse<bool>(false,language.Failled,2,true);

            await _unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse<bool>(true,language.Deleted,1,false);
        }
    }
}