using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Exceptions;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service;
using MobileScooter.Api.Service.Crypto;
using MobileScooter.Api.Service.Repositories.Abstract;

namespace MobileScooter.Api.Controllers
{
    [ApiController]
    [Route("account")]
    public class AccountController : ControllerBase
    {

        private readonly IUnitOfWork unitOfWork;
        private readonly ILanguage language;

        public AccountController(IUnitOfWork unitOfWork, ILanguage language)
        {
            this.unitOfWork = unitOfWork;
            this.language = language;
        }

        [HttpPost("login")]
        public async Task<ApiResponse<string>> Login([FromBody] LoginModel model)
        {
            var result = await unitOfWork.AccountRepository.Login(model);

            return ResponseHelper.CreateApiResponse(result,language.Success,1,false);
        }

        [HttpPost("register")]
        public async Task<ApiResponse<User>> Register([FromBody] RegisterModel model){

            var user = await unitOfWork.AccountRepository.Register(model);
            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse<User>(user,language.Created,1,false);
        }

        [HttpPost("password/refresh")]
        public async Task<ApiResponse<bool>> RefreshPassoword([FromBody] RefreshPasswordModel model)
        {
            var user = await unitOfWork.AccountRepository.RefreshPassword(model);
            
            unitOfWork.UserRepository.Update(user);
            await unitOfWork.CommitAsync();

            return ResponseHelper.CreateApiResponse<bool>(true,language.Success,1,false);
        }

        [HttpPost("password/forgot")]
        public async Task<ApiResponse<bool>> ForgotPassword([FromBody] ForgotPasswordModel model){

             var userResponse = unitOfWork.UserRepository.GetByEmail(model.Email);
             if(userResponse == null)
                return ResponseHelper.CreateApiResponse<bool>(false,language.UserNotFound,2,true);
            if(userResponse.Identity != model.Identity)
                return ResponseHelper.CreateApiResponse<bool>(false,language.WrongInput,2,true);

            var verificationKey = await unitOfWork.AccountRepository.ForgotPasswordSendEmail(model); 
            // result.Data yı VerificationPasswords Table a kaydet.
            var VerificationPasswords = new VerificationPassword{UserEmail=model.Email,VerificationCode=verificationKey};
            var verificationPasswordResult = await unitOfWork.VerificationPasswordRepository.InsertAsync(VerificationPasswords);
            if(verificationPasswordResult == null)
                return ResponseHelper.CreateApiResponse<bool>(false,language.Failled,2,true);
            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse<bool>(true,language.Success,1,false);
        }

        [HttpPost("password/reset")]
        public async Task<ApiResponse<bool>> ResetPassword([FromBody] ResetPasswordModel model)
        {
            var verificationCode = unitOfWork.VerificationPasswordRepository.GetByUserEmail(model.Email);
            if(verificationCode == null)
                ResponseHelper.CreateApiResponse<bool>(false,language.NotFound,2,true);

            if(!(verificationCode.VerificationCode == model.VerificationCode))
                return ResponseHelper.CreateApiResponse<bool>(false,language.WrongInput,2,true);

            // dogrulama islemi basarılı yeni sifre atanabilir
            var user = await unitOfWork.AccountRepository.ResetPassword(model);
            unitOfWork.UserRepository.Update(user);
            await unitOfWork.VerificationPasswordRepository.DeleteAsync(verificationCode);
            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse<bool>(true,language.Success,1,false);        
        }
    }
}