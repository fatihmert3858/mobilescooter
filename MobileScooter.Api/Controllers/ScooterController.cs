using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service;

namespace MobileScooter.Api.Controllers
{
    [ApiController]
    [Route("scooter")]
    public class ScooterController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILanguage language;

        public ScooterController(IUnitOfWork unitOfWork, ILanguage language)
        {
            this.unitOfWork = unitOfWork;
            this.language = language;
        }

        [HttpGet("all")]
        public async Task<ApiResponse<IList<Scooter>>> GetAll()
        {
            var result = await unitOfWork.ScooterRepository.GetAllAsync();
            return ResponseHelper.CreateApiResponse<IList<Scooter>>(result,language.Success,1,false);
        }

        [HttpGet("barcode/{id:guid}")]
        public async Task<ApiResponse<string>> GetBarcode(string id)
        {
            var barcode = await unitOfWork.ScooterRepository.GetBarcode(id);
            return ResponseHelper.CreateApiResponse(barcode,language.Success,1,false);
        }
        [HttpGet("barcode/{number:int:min(1)}")]
        public async Task<ApiResponse<string>> GetBarcode(int number)
        {
            var barcode = await unitOfWork.ScooterRepository.GetBarcode(number);
            return ResponseHelper.CreateApiResponse(barcode,language.Success,1,false);
        }
        [HttpGet("number/{number:int:min(1)}")]
        public async Task<ApiResponse<Scooter>> GetByNumber(int number)
        {
            var scooter = await unitOfWork.ScooterRepository.GetByNumber(number);
            return ResponseHelper.CreateApiResponse(scooter,language.Success,1,false);
        }
        [HttpGet("charge/{number:int:min(1)}")]
        public async Task<ApiResponse<int>> GetChargeState(int number)
        {
            var chargeState = await unitOfWork.ScooterRepository.GetChargeState(number);
            return ResponseHelper.CreateApiResponse(chargeState,language.Success,1,false);
        }
        [HttpGet("charge/{id:guid}")]
        public async Task<ApiResponse<int>> GetChargeState(string id)
        {
            var chargeState = await unitOfWork.ScooterRepository.GetChargeState(id);
            return ResponseHelper.CreateApiResponse(chargeState,language.Success,1,false);
        }
        [HttpGet("count")]
        public async Task<ApiResponse<int>> GetCount()
        {
            var count = await unitOfWork.ScooterRepository.GetCount();
            return ResponseHelper.CreateApiResponse(count,language.Success,1,false);
        }
        [HttpGet("location/{id:guid}")]
        public async Task<ApiResponse<string>> GetLocation(string id)
        {
            var location = await unitOfWork.ScooterRepository.GetLocation(id);
            return ResponseHelper.CreateApiResponse(location,language.Success,1,false);
        }
        [HttpGet("location/{number:int:min(1)}")]
        public async Task<ApiResponse<string>> GetLocation(int number)
        {
            var location = await unitOfWork.ScooterRepository.GetLocation(number);
            return ResponseHelper.CreateApiResponse(location,language.Success,1,false);
        }
        
        [Authorize("TECNICIAN,ADMIN")]
        [HttpPost("add")]
        public async Task<ApiResponse<Scooter>> Add([FromBody] Scooter scooter)
        {
            var result = await unitOfWork.ScooterRepository.InsertAsync(scooter);
            if(result == null)
                return ResponseHelper.CreateApiResponse(result,language.Failled,2,true);
            
            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse(result,language.Created,1,false);
        }
        [Authorize("TECHNICIAN,ADMIN")]
        [HttpPost("update")]
        public async Task<ApiResponse<Scooter>> Update([FromBody] Scooter scooter)
        {
            var result = unitOfWork.ScooterRepository.Update(scooter);
            if(result == null)
                return ResponseHelper.CreateApiResponse<Scooter>(null,language.Failled,2,true);
            
            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse(result,language.Created,1,false);
        }

        [Authorize("TECHNICIAN,ADMIN")]
        [HttpPost("delete")]
        public async Task<ApiResponse<bool>> Delete([FromBody] Scooter scooter)
        {
            var result = await unitOfWork.ScooterRepository.DeleteAsync(scooter);
            if(result == false)
                return ResponseHelper.CreateApiResponse<bool>(false,language.WrongInput,2,true);

            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse(result,language.Success,1,false);
        }
        
        [Authorize("TECHNICIAN,ADMIN")]
        [HttpPost("delete/{id:guid}")]
        public async Task<ApiResponse<bool>> Delete(string id)
        {
            var result = await unitOfWork.ScooterRepository.DeleteAsync(id);
            if(result == false)
                return ResponseHelper.CreateApiResponse<bool>(false,language.WrongInput,2,true);

            await unitOfWork.CommitAsync();
            return ResponseHelper.CreateApiResponse(result,language.Success,1,false);
        }

    }
}