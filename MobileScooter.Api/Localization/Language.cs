namespace MobileScooter.Api.Localization
{
    public class Language : ILanguage
    {

        private readonly Translator translator;

        public Language(Translator translator)
        {
            this.translator = translator;
        }

        // Fails
        public string Failled => translator.Translate(nameof(Failled));
        public string AlreadyCreated => translator.Translate(nameof(AlreadyCreated));
        // Errors
        public string WrongInput => translator.Translate(nameof(WrongInput));
        public string WrongPassword => translator.Translate(nameof(WrongPassword));
        public string NoAuth => translator.Translate(nameof(NoAuth));
        public string NotFound => translator.Translate(nameof(NotFound));
        public string UserNotFound => translator.Translate(nameof(UserNotFound));
        public string ListEmptyOrNull => translator.Translate(nameof(ListEmptyOrNull));
        //Info
        public string Deleted => translator.Translate(nameof(Deleted));
        // Success
        public string Success => translator.Translate(nameof(Success));
        public string Created => translator.Translate(nameof(Created));
        public string Updated => translator.Translate(nameof(Updated));
        
    }


    public interface ILanguage
    {
        // Fails
        string Failled { get; }
        string AlreadyCreated { get; } 

        // Errors
        string WrongInput { get; }
        string WrongPassword {get;}
        string NoAuth {get;}
        string NotFound {get;}
        string UserNotFound { get; }
        string ListEmptyOrNull {get;}

        // Warnings

        // Info
        string Deleted {get;}

        // Success
        string Success { get; }
        string Created {get;}
        string Updated {get;}
    }
}