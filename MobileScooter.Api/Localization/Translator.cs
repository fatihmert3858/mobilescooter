using Microsoft.Extensions.Localization;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Localization
{
    public class Translator
    {
        private readonly IStringLocalizer<SharedResource> localizer;

        public Translator(IStringLocalizer<SharedResource> localizer)
        {
            this.localizer = localizer;
        }


        public string Translate(string title)
        {
            return localizer[title];
        }
    }
}