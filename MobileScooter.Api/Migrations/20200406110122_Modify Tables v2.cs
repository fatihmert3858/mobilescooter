﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MobileScooter.Api.Migrations
{
    public partial class ModifyTablesv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories");

            migrationBuilder.AlterColumn<string>(
                name: "to_station_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AddPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories");

            migrationBuilder.AlterColumn<string>(
                name: "to_station_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories",
                columns: new[] { "id", "user_id", "scooter_id", "from_station_id", "to_station_id" });
        }
    }
}
