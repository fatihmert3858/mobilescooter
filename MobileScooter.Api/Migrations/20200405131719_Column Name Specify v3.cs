﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MobileScooter.Api.Migrations
{
    public partial class ColumnNameSpecifyv3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_active_scooters_scooters_scooter_id",
                table: "active_scooters");

            migrationBuilder.DropForeignKey(
                name: "fk_active_scooters_users_user_id",
                table: "active_scooters");

            migrationBuilder.DropForeignKey(
                name: "fk_payments_users_user_id",
                table: "payments");

            migrationBuilder.DropForeignKey(
                name: "fk_payments_scooter_transport_histories_scooter_transport_hist",
                table: "payments");

            migrationBuilder.DropIndex(
                name: "ix_payments_user_id",
                table: "payments");

            migrationBuilder.DropIndex(
                name: "ix_payments_scooter_transport_history_id1_scooter_transport_hi",
                table: "payments");

            migrationBuilder.DropIndex(
                name: "ix_active_scooters_scooter_id",
                table: "active_scooters");

            migrationBuilder.DropIndex(
                name: "ix_active_scooters_user_id",
                table: "active_scooters");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_from_station_id",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_id1",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_scooter_id",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_to_station_id",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_user_id",
                table: "payments");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_from_station_id",
                table: "payments",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_id1",
                table: "payments",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_scooter_id",
                table: "payments",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_to_station_id",
                table: "payments",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_user_id",
                table: "payments",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_payments_user_id",
                table: "payments",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ix_payments_scooter_transport_history_id1_scooter_transport_hi",
                table: "payments",
                columns: new[] { "scooter_transport_history_id1", "scooter_transport_history_user_id", "scooter_transport_history_scooter_id", "scooter_transport_history_from_station_id", "scooter_transport_history_to_station_id" });

            migrationBuilder.CreateIndex(
                name: "ix_active_scooters_scooter_id",
                table: "active_scooters",
                column: "scooter_id");

            migrationBuilder.CreateIndex(
                name: "ix_active_scooters_user_id",
                table: "active_scooters",
                column: "user_id");

            migrationBuilder.AddForeignKey(
                name: "fk_active_scooters_scooters_scooter_id",
                table: "active_scooters",
                column: "scooter_id",
                principalTable: "scooters",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_active_scooters_users_user_id",
                table: "active_scooters",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_payments_users_user_id",
                table: "payments",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_payments_scooter_transport_histories_scooter_transport_hist",
                table: "payments",
                columns: new[] { "scooter_transport_history_id1", "scooter_transport_history_user_id", "scooter_transport_history_scooter_id", "scooter_transport_history_from_station_id", "scooter_transport_history_to_station_id" },
                principalTable: "scooter_transport_histories",
                principalColumns: new[] { "id", "user_id", "scooter_id", "from_station_id", "to_station_id" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
