﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MobileScooter.Api.Migrations
{
    public partial class ModifyTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_from_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooters_scooter_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_to_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_users_user_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooter_transport_histories_from_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooter_transport_histories_scooter_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooter_transport_histories_to_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooter_transport_histories_user_id",
                table: "scooter_transport_histories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "ix_scooter_transport_histories_from_station_id",
                table: "scooter_transport_histories",
                column: "from_station_id");

            migrationBuilder.CreateIndex(
                name: "ix_scooter_transport_histories_scooter_id",
                table: "scooter_transport_histories",
                column: "scooter_id");

            migrationBuilder.CreateIndex(
                name: "ix_scooter_transport_histories_to_station_id",
                table: "scooter_transport_histories",
                column: "to_station_id");

            migrationBuilder.CreateIndex(
                name: "ix_scooter_transport_histories_user_id",
                table: "scooter_transport_histories",
                column: "user_id");

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_from_station_id",
                table: "scooter_transport_histories",
                column: "from_station_id",
                principalTable: "scooter_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooters_scooter_id",
                table: "scooter_transport_histories",
                column: "scooter_id",
                principalTable: "scooters",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_to_station_id",
                table: "scooter_transport_histories",
                column: "to_station_id",
                principalTable: "scooter_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_users_user_id",
                table: "scooter_transport_histories",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
