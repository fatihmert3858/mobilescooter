﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MobileScooter.Api.Migrations
{
    public partial class ColumnNameSpecifyv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_active_scooters_scooters_scooter_id",
                table: "active_scooters");

            migrationBuilder.DropForeignKey(
                name: "fk_active_scooters_users_user_id",
                table: "active_scooters");

            migrationBuilder.DropForeignKey(
                name: "fk_energy_generators_energy_stations_energy_station_id",
                table: "energy_generators");

            migrationBuilder.DropForeignKey(
                name: "fk_payments_scooter_transport_histories_scooter_transport_hist",
                table: "payments");

            migrationBuilder.DropForeignKey(
                name: "fk_payments_users_user_id",
                table: "payments");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_from_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooters_scooter_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_to_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_users_user_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_payments_scooter_transport_history_id",
                table: "payments");

            migrationBuilder.AlterColumn<int>(
                name: "number",
                table: "scooters",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "max_range",
                table: "scooters",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<string>(
                name: "location",
                table: "scooters",
                type: "varchar",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "current_range",
                table: "scooters",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "charge_state",
                table: "scooters",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<string>(
                name: "barcode",
                table: "scooters",
                type: "varchar",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "scooters",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "to_station_in_time",
                table: "scooter_transport_histories",
                type: "timestamp",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<string>(
                name: "to_station_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "scooter_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "from_station_out_time",
                table: "scooter_transport_histories",
                type: "timestamp",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<string>(
                name: "from_station_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "location",
                table: "scooter_stations",
                type: "varchar",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "scooter_stations",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "payments",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "scooter_transport_history_id",
                table: "payments",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "payments",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_from_station_id",
                table: "payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_id1",
                table: "payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_scooter_id",
                table: "payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_to_station_id",
                table: "payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "scooter_transport_history_user_id",
                table: "payments",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "energy_stations",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "type",
                table: "energy_generators",
                type: "varchar(24)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "energy_station_id",
                table: "energy_generators",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "energy_generators",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "active_scooters",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "scooter_id",
                table: "active_scooters",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "active_scooters",
                type: "varchar(36)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories",
                columns: new[] { "id", "user_id", "scooter_id", "from_station_id", "to_station_id" });

            migrationBuilder.CreateIndex(
                name: "ix_scooters_id",
                table: "scooters",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_scooter_transport_histories_id",
                table: "scooter_transport_histories",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_scooter_stations_id",
                table: "scooter_stations",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_payments_id",
                table: "payments",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_payments_scooter_transport_history_id1_scooter_transport_hi",
                table: "payments",
                columns: new[] { "scooter_transport_history_id1", "scooter_transport_history_user_id", "scooter_transport_history_scooter_id", "scooter_transport_history_from_station_id", "scooter_transport_history_to_station_id" });

            migrationBuilder.CreateIndex(
                name: "ix_energy_stations_id",
                table: "energy_stations",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_energy_generators_id",
                table: "energy_generators",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_active_scooters_id",
                table: "active_scooters",
                column: "id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_active_scooters_scooters_scooter_id",
                table: "active_scooters",
                column: "scooter_id",
                principalTable: "scooters",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_active_scooters_users_user_id",
                table: "active_scooters",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_energy_generators_energy_stations_energy_station_id",
                table: "energy_generators",
                column: "energy_station_id",
                principalTable: "energy_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_payments_users_user_id",
                table: "payments",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_payments_scooter_transport_histories_scooter_transport_hist",
                table: "payments",
                columns: new[] { "scooter_transport_history_id1", "scooter_transport_history_user_id", "scooter_transport_history_scooter_id", "scooter_transport_history_from_station_id", "scooter_transport_history_to_station_id" },
                principalTable: "scooter_transport_histories",
                principalColumns: new[] { "id", "user_id", "scooter_id", "from_station_id", "to_station_id" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_from_station_id",
                table: "scooter_transport_histories",
                column: "from_station_id",
                principalTable: "scooter_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooters_scooter_id",
                table: "scooter_transport_histories",
                column: "scooter_id",
                principalTable: "scooters",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_to_station_id",
                table: "scooter_transport_histories",
                column: "to_station_id",
                principalTable: "scooter_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_users_user_id",
                table: "scooter_transport_histories",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_active_scooters_scooters_scooter_id",
                table: "active_scooters");

            migrationBuilder.DropForeignKey(
                name: "fk_active_scooters_users_user_id",
                table: "active_scooters");

            migrationBuilder.DropForeignKey(
                name: "fk_energy_generators_energy_stations_energy_station_id",
                table: "energy_generators");

            migrationBuilder.DropForeignKey(
                name: "fk_payments_users_user_id",
                table: "payments");

            migrationBuilder.DropForeignKey(
                name: "fk_payments_scooter_transport_histories_scooter_transport_hist",
                table: "payments");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_from_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooters_scooter_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_to_station_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropForeignKey(
                name: "fk_scooter_transport_histories_users_user_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooters_id",
                table: "scooters");

            migrationBuilder.DropPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooter_transport_histories_id",
                table: "scooter_transport_histories");

            migrationBuilder.DropIndex(
                name: "ix_scooter_stations_id",
                table: "scooter_stations");

            migrationBuilder.DropIndex(
                name: "ix_payments_id",
                table: "payments");

            migrationBuilder.DropIndex(
                name: "ix_payments_scooter_transport_history_id1_scooter_transport_hi",
                table: "payments");

            migrationBuilder.DropIndex(
                name: "ix_energy_stations_id",
                table: "energy_stations");

            migrationBuilder.DropIndex(
                name: "ix_energy_generators_id",
                table: "energy_generators");

            migrationBuilder.DropIndex(
                name: "ix_active_scooters_id",
                table: "active_scooters");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_from_station_id",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_id1",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_scooter_id",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_to_station_id",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "scooter_transport_history_user_id",
                table: "payments");

            migrationBuilder.AlterColumn<int>(
                name: "number",
                table: "scooters",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "max_range",
                table: "scooters",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "location",
                table: "scooters",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar");

            migrationBuilder.AlterColumn<int>(
                name: "current_range",
                table: "scooters",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "charge_state",
                table: "scooters",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "barcode",
                table: "scooters",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "scooters",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "to_station_in_time",
                table: "scooter_transport_histories",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp");

            migrationBuilder.AlterColumn<DateTime>(
                name: "from_station_out_time",
                table: "scooter_transport_histories",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp");

            migrationBuilder.AlterColumn<string>(
                name: "to_station_id",
                table: "scooter_transport_histories",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "from_station_id",
                table: "scooter_transport_histories",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "scooter_id",
                table: "scooter_transport_histories",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "scooter_transport_histories",
                type: "varchar(36)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "scooter_transport_histories",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "location",
                table: "scooter_stations",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "scooter_stations",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "payments",
                type: "varchar(36)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "scooter_transport_history_id",
                table: "payments",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "payments",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "energy_stations",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "type",
                table: "energy_generators",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(24)");

            migrationBuilder.AlterColumn<string>(
                name: "energy_station_id",
                table: "energy_generators",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "energy_generators",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "active_scooters",
                type: "varchar(36)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "scooter_id",
                table: "active_scooters",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "active_scooters",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(36)");

            migrationBuilder.AddPrimaryKey(
                name: "pk_scooter_transport_histories",
                table: "scooter_transport_histories",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "ix_payments_scooter_transport_history_id",
                table: "payments",
                column: "scooter_transport_history_id");

            migrationBuilder.AddForeignKey(
                name: "fk_active_scooters_scooters_scooter_id",
                table: "active_scooters",
                column: "scooter_id",
                principalTable: "scooters",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_active_scooters_users_user_id",
                table: "active_scooters",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_energy_generators_energy_stations_energy_station_id",
                table: "energy_generators",
                column: "energy_station_id",
                principalTable: "energy_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_payments_scooter_transport_histories_scooter_transport_hist",
                table: "payments",
                column: "scooter_transport_history_id",
                principalTable: "scooter_transport_histories",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_payments_users_user_id",
                table: "payments",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_from_station_id",
                table: "scooter_transport_histories",
                column: "from_station_id",
                principalTable: "scooter_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooters_scooter_id",
                table: "scooter_transport_histories",
                column: "scooter_id",
                principalTable: "scooters",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_scooter_stations_to_station_id",
                table: "scooter_transport_histories",
                column: "to_station_id",
                principalTable: "scooter_stations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_scooter_transport_histories_users_user_id",
                table: "scooter_transport_histories",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
