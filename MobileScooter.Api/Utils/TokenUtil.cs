using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Utils
{
    public static class TokenUtil
    {
        public static IEnumerable<Claim> ParseToken(string token){
            if(token.Equals("") || token == null)
                return null;
            IEnumerable<Claim> result = null;
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = new JwtSecurityToken(token);
                result = jwtToken.Claims;
            }catch(System.Exception e){
                System.Console.WriteLine(e.Message);
            }
            return result;
        }

        public static string CreateToken(string userId,string userName, byte[] key, string role = Role.STUDENT){
           var claims = new List<Claim>{
                new Claim("userId",userId),
                new Claim(ClaimTypes.Name,userName),
                new Claim(ClaimTypes.Role, role)
            };
            var encodedKey = new SymmetricSecurityKey(key);
            var creds = new SigningCredentials(encodedKey,SecurityAlgorithms.HmacSha256Signature);
            var expires = DateTime.Now.AddDays(3);
            
            var token = new JwtSecurityToken("http://erumobilescooter.com","http://erumobilescooter.com",claims,
                expires:expires,signingCredentials:creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public static async Task<string> CreateTokenAsync(string userId,string userName,string role = Role.STUDENT){

            var key = Encoding.ASCII.GetBytes(AppSettings.Secret);
            var claims = new List<Claim>{
                new Claim("userId",userId),
                new Claim(ClaimTypes.Name,userName),
                new Claim(ClaimTypes.Role, role)
            };
            var encodedKey = new SymmetricSecurityKey(key);
            var creds = new SigningCredentials(encodedKey,SecurityAlgorithms.HmacSha256Signature);
            var expires = DateTime.Now.AddDays(3);
            
            var token = new JwtSecurityToken("http://erumobilescooter.com","http://erumobilescooter.com",claims,
                expires:expires,signingCredentials:creds);
            var result =  await Task.Run( () => new JwtSecurityTokenHandler().WriteToken(token));
            return result;
        }
    }
}