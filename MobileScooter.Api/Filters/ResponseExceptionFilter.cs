﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using MobileScooter.Api.Exceptions;

namespace MobileScooter.Api.Filters
{
    public class ResponseExceptionFilter : IAsyncExceptionFilter
    {
        public Task OnExceptionAsync(ExceptionContext context)
        {
            if(context.Exception is ResponseException)
            {
                var responseException = context.Exception;
                context.Result = new Models.ValidatorResponse(new Models.ApiResponse<object>{Message=responseException.Message,Error=true,StatusCode=3});
            }
            return Task.CompletedTask;
        }
    }
}