using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Filters
{
    public class ModelValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if(!context.ModelState.IsValid)
            {
                var dict = context.ModelState as ModelStateDictionary;
                var result = dict.FirstOrDefault().Value?.Errors.FirstOrDefault().ErrorMessage;
                if(!string.IsNullOrEmpty(result))
                {
                    var response = new ValidatorResponse(new ApiResponse<object>{Data=default,Error=true,Message=result,StatusCode=2});
                    context.Result = response;
                    return;
                }
            }
            await next();
        }
    }
}