using System;

namespace MobileScooter.Api.Exceptions
{
    public class ResponseException: Exception
    {
        
        public ResponseException(string message) : base(message)
        {
        }
    }
}