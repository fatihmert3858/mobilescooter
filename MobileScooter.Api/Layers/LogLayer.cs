using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MobileScooter.Api.Layers
{
    public class LogLayer
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LogLayer> logger;


        public LogLayer(RequestDelegate requestDelegate, ILogger<LogLayer> logger)
        {
            _next = requestDelegate;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            finally
            {
                logger.LogInformation("Request Headers {headers}\nRequest {method}\n{url}\n{statusCode}",
                                    context.Request?.Headers,context.Request?.Method, context.Request?.Path.Value, context.Response?.StatusCode);
            }
        } 
    }
}