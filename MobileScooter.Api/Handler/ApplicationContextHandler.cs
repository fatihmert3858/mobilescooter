using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MobileScooter.Api.Handler
{
    public class ApplicationContextHandler
    {
        public static IDictionary<string,string> CommitHandle(System.Exception exception){
            IDictionary<string,string> result = new Dictionary<string,string>();
            if(exception is DbUpdateConcurrencyException){
                var ex = exception as DbUpdateConcurrencyException;
                Splitter(result, ex.InnerException.ToString());
            }
            else if(exception is DbUpdateException){
                var ex = exception as DbUpdateException;
                Splitter(result, ex.InnerException.ToString());
            } 
            return result;
        }

        private static void Splitter(IDictionary<string,string> dict,string innerEx)
        {
            var exceptionDataStart = innerEx.IndexOf("Exception data:\r\n");
            if(exceptionDataStart == -1 )
                return;
            var message = innerEx.Substring(exceptionDataStart);
            var detailIndex = message.IndexOf("Detail");
            if(detailIndex < 0)
                return;
            var detailLast = message.Substring(detailIndex);
            var detail = detailLast.Split('\r')[0];
            detail = detail.Replace('(',' ');
            detail = detail.Replace(')',' ');
            detail = detail.Remove(0,13);
            dict.Add("DeveloperEx",message);
            dict.Add("UserEx",detail);
        }
    }
}