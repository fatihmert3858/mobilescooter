using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MobileScooter.Api.Data;
using MobileScooter.Api.Filters;
using MobileScooter.Api.Helpers;
using MobileScooter.Api.Layers;
using MobileScooter.Api.Localization;
using MobileScooter.Api.Models;
using MobileScooter.Api.Service;
using MobileScooter.Api.Service.Email;
using MobileScooter.Api.Service.Repositories.Abstract;
using MobileScooter.Api.Service.Repositories.Concreate;

namespace MobileScooter.Api
{
    // mobile-scooter-db
    //aNL6CNmeXBBvmp3JAuM3fvTqyZudkf
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(o => o.ResourcesPath = "Resources");

            services.AddControllers();
            
            
            services.AddDbContext<ApplicationContext>(options => {
                options.UseNpgsql(Configuration.GetConnectionString("postgresConString"))
                    .UseSnakeCaseNamingConvention();
            });

            services.AddAuthorization(options => {
                options.AddPolicy("ADMIN",builder => {
                    builder.RequireRole("Administrator");
                });
            });
            
            ConfigureJwt(services);

            services.AddMvc(opt =>{
                opt.EnableEndpointRouting =false;    
                opt.Filters.Add<ModelValidationFilter>();
                opt.Filters.Add<ResponseExceptionFilter>();
            }).AddDataAnnotationsLocalization(options => {
                options.DataAnnotationLocalizerProvider = (type,factory) => {
                    return factory.Create(typeof(SharedResource));
                };
            });
            
            services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);

            services.AddSingleton<ILanguage, Language>();
            services.AddSingleton<Translator>();
            services.AddScoped<ApplicationContext>();
            services.AddScoped<IUnitOfWork,UnitOfWork>();
            services.AddScoped<IAccountRepository,AccountRepository>();
          //  services.AddTransient<IEmailService,SendGridEmailSevice>();
            services.AddTransient<IEmailService,SmtpEmailService>();

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            UpdateDatabase(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<LogLayer>();

            app.UseHttpsRedirection();
            
            app.UseStaticFiles();

            var supportedCultures = new List<CultureInfo>{ new CultureInfo("en-US"), new CultureInfo("tr-TR") };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("tr-TR"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
            app.Use(async (context,next) => {
                await next();
                var language = app.ApplicationServices.GetService<ILanguage>();
                if(context.Response.StatusCode == 401){
                    var apiResponse = ResponseHelper.CreateApiResponse<object>(null,language.NoAuth,4,true);
                    var json = JsonSerializer.Serialize(apiResponse);
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(json);
            }
            });
            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

           app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseMvc();

        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ApplicationContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
        
        private void ConfigureJwt(IServiceCollection services)
        {
            var key = Encoding.ASCII.GetBytes(AppSettings.Secret);
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication( x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x => {
                // Requestlerin sadece HTTPS yani SSL sertifikası olanları kabul etmesi
                x.RequireHttpsMetadata = false;
                // Onaylanan Tokenları Sunucu Tarafına Kayıt eder.
                x.SaveToken = true;
                // Token Icerisinde Nelerin kontrol Edilecegi.
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters{
                    ValidIssuer = "http://erumobilescooter.com",
                    ValidAudience = "http://erumobilescooter.com",
                    // Ne İle Kontrol Etmesi Gerektigi
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ClockSkew = TimeSpan.Zero
                };
            });
        }

    }
}
