using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using MobileScooter.Api.Data.Entities;

namespace MobileScooter.Api.Data
{
        public class ApplicationContext: DbContext {
        public DbSet<ActiveScooter> ActiveScooters {get; set;}
        public DbSet<EnergyGenerator> EnergyGenerators { get; set; }
        public DbSet<EnergyStation> EnergyStations { get; set; }
        public DbSet<Scooter> Scooters { get; set; }
        public DbSet<ScooterTransportHistory> ScooterTransportHistories { get; set; }
        public DbSet<ScooterStation> ScooterStations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<VerificationPassword> VerificationPasswords { get; set; }

     
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) {
             this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
             this.ChangeTracker.LazyLoadingEnabled = false;
             this.ChangeTracker.AutoDetectChangesEnabled = false;
        }
        public static readonly ILoggerFactory loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseLoggerFactory(loggerFactory).EnableSensitiveDataLogging();
        }
     

        protected override void OnModelCreating(ModelBuilder modelBuilder){
            base.OnModelCreating(modelBuilder);
            ConfigureTables(modelBuilder);
        }

        private void ConfigureTables(ModelBuilder modelBuilder)
        {
            UserTable(modelBuilder);
            ActiveScooterTable(modelBuilder);
            EnergyGeneratorsTable(modelBuilder);
            EnergyStationsTable(modelBuilder);
            ScooterTable(modelBuilder);
            ScooterTransportsHistoriesTable(modelBuilder);
            ScooterStationsTable(modelBuilder);
            PaymentsTable(modelBuilder);
        }

        private void UserTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(u =>
            {
                u.ToTable("users");
                u.HasIndex(x => x.Email).IsUnique();
                u.HasIndex(x => x.Identity).IsUnique();
                u.HasIndex(x => x.Id).IsUnique();
                u.HasKey(x => x.Id);

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.Name).HasColumnName("name").HasColumnType("varchar(60)").IsRequired();
                u.Property(x => x.Surname).HasColumnName("surname").HasColumnType("varchar(50)").IsRequired();
                u.Property(x => x.Email).HasColumnName("email").HasColumnType("varchar(120)").IsRequired();
                u.Property(x => x.Phone).HasColumnName("phone").HasColumnType("varchar(10)").IsRequired();
                u.Property(x => x.Identity).HasColumnName("identity").HasColumnType("varchar(11)").IsRequired();
                u.Property(x => x.Role).HasColumnName("role").HasColumnType("varchar(24)").IsRequired();
                u.Property(x => x.Faculty).HasColumnName("faculty").HasColumnType("varchar(255)").IsRequired();
                u.Property(x => x.Department).HasColumnName("department").HasColumnType("varchar(255)").IsRequired();
                u.Property(x => x.PasswordHash).HasColumnName("password_hash").HasColumnType("varchar").IsRequired();
                u.Property(x => x.Gender).HasColumnName("gender").HasColumnType("varchar(24)").HasDefaultValue(Gender.NONE);
                u.Property(x => x.isBanned).HasColumnName("is_banned").HasColumnType("boolean").HasDefaultValue(false);

            });
        }

        private void ActiveScooterTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActiveScooter>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(x => x.Id).IsUnique();
                u.ToTable("active_scooters");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.UserId).HasColumnName("user_id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.ScooterId).HasColumnName("scooter_id").HasColumnType("varchar(36)").IsRequired();
            });
        }

        private void EnergyGeneratorsTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EnergyGenerator>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(x => x.Id).IsUnique();
                u.ToTable("energy_generators");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.EnergyCapacity).HasColumnName("energy_capacity").HasColumnType("integer").IsRequired();
                u.Property(x => x.CurrentEnergy).HasColumnName("current_energy").HasColumnType("integer");
                u.Property(x => x.Type).HasColumnName("type").HasColumnType("varchar(24)").IsRequired();
                u.Property(x => x.EnergyStationId).HasColumnName("energy_station_id").HasColumnType("varchar(36)").IsRequired();

                u.HasOne<EnergyStation>(x => x.EnergyStation)
                    .WithMany( e => e.EnergyGenerators)
                    .HasForeignKey(x => x.EnergyStationId);
            });
        }
        private void EnergyStationsTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EnergyStation>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(x => x.Id).IsUnique();
                u.ToTable("energy_stations");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.EnergyCapacity).HasColumnName("energy_capacity").HasColumnType("double precision").IsRequired();
                u.Property(x => x.CurrentEnergy).HasColumnName("current_energy").HasColumnType("double precision").IsRequired();
                
            });
        }
        private void ScooterTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Scooter>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(x => x.Id).IsUnique();
                u.ToTable("scooters");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.ChargeState).HasColumnName("charge_state").HasColumnType("int").IsRequired();
                u.Property(x => x.Number).HasColumnName("number").HasColumnType("int").IsRequired();
                u.Property(x => x.MaxRange).HasColumnName("max_range").HasColumnType("int").IsRequired();
                u.Property(x => x.Barcode).HasColumnName("barcode").HasColumnType("varchar").IsRequired();
                u.Property(x => x.CurrentRange).HasColumnName("current_range").HasColumnType("int").IsRequired();
                u.Property(x => x.Location).HasColumnName("location").HasColumnType("varchar").IsRequired();
            });
        }
        private void ScooterTransportsHistoriesTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScooterTransportHistory>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(x => x.Id).IsUnique();
                u.ToTable("scooter_transport_histories");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.ScooterId).HasColumnName("scooter_id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.FromStationId).HasColumnName("from_station_id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.ToStationId).HasColumnName("to_station_id").HasColumnType("varchar(36)");
                u.Property(x => x.FromStationOutTime).HasColumnName("from_station_out_time").HasColumnType("timestamp").IsRequired();
                u.Property(x => x.ToStationInTime).HasColumnName("to_station_in_time").HasColumnType("timestamp");
                u.Property(x => x.UserId).HasColumnName("user_id").HasColumnType("varchar(36)").IsRequired();
                
            });
        }

        private void ScooterStationsTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScooterStation>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(x => x.Id).IsUnique();
                u.ToTable("scooter_stations");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.MaxCapacity).HasColumnName("max_capacity").HasColumnType("integer").IsRequired();
                u.Property(x => x.CurrentCapacity).HasColumnName("current_capacity").HasColumnType("integer").IsRequired();
                u.Property(x => x.Location).HasColumnName("location").HasColumnType("varchar").IsRequired();
            });
        }
        private void PaymentsTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>(u =>
            {
                u.HasKey(s => s.Id);
                u.HasIndex(s => s.Id).IsUnique();
                u.ToTable("payments");

                u.Property(x => x.Id).HasColumnName("id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.Price).HasColumnName("price").HasColumnType("double precision").IsRequired();
                u.Property(x => x.UserId).HasColumnName("user_id").HasColumnType("varchar(36)").IsRequired();
                u.Property(x => x.ScooterTransportHistoryId).HasColumnName("scooter_transport_history_id").HasColumnType("varchar(36)").IsRequired();
                
            });
        }
    }
}