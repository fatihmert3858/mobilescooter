using System;
using EruMobileScooter.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MobileScooter.Api.Data.Entities
{
    public class User : BaseEntity {

        [Required(ErrorMessage="RequiredName")]
        public string Name { get; set; }
        [Required(ErrorMessage = "RequiredSurname")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "RequiredEmail")]
        public string Email { get; set; }   // Öğrenci veya Öğretmen mail adresi. 1031320314@erciyes.edu.tr
        [Required(ErrorMessage = "RequiredRole")]
        public string Role { get; set; }    // Öğretmen veya Öğrenci
        [Required(ErrorMessage = "RequiredFaculty")]
        public string Faculty { get; set; } // Mensup oldugu Fakülte. Mühendislik Fakültesi
        [Required(ErrorMessage = "RequiredDepartment")]
        public string Department { get; set; }  // Fakülte Bölümü. Enerji Sistemleri Mühendisligi
        [Required(ErrorMessage = "RequiredIdentity")]
        public string Identity { get; set; }    // Türkiye Cumhuriyeti kimlik Numarası
        [Required(ErrorMessage = "RequiredPassword")]
        public string PasswordHash { get; set; }
        [Required(ErrorMessage = "RequiredGender")]
        public string Gender { get; set; }  // Cinsiyet
        [Required(ErrorMessage = "RequiredPhone")]
        public string Phone { get; set; }
        public bool isBanned { get; set; }  // Kullanıcının banlanıp banlanmadıgı 
    }


    public static class Gender {
        public const string NONE = "NONE";   // Tanımlanmamıs
        public const string MALE = "MALE";   // erkek
        public const string FEMALE = "FEMALE"; // kadın
        public const string UNISEX = "UNISEX"; // Eşcinsel
        public const string DONT_WANT_TO = "DONT WANT TO"; // Belirtmek İstemiyorum
    }
    public static class Role {
        public const string None = "NONE";
        public const string TECHNICIAN ="TECHNICIAN";
        public const string TEACHER = "TEACHER";
        public const string STUDENT = "STUDENT";
        public const string ADMIN = "ADMIN";
    }
}