using System;
using EruMobileScooter.Data.Entities;

namespace MobileScooter.Api.Data.Entities
{
       public class ScooterTransportHistory : BaseEntity {

        /**
        *   User Foreign Key
        */
        public string UserId { get; set; }
        /**
        * Scooter Foreign Key
        */
        public string ScooterId { get; set; }
        /**
        *   FromScooter Foreign Key
        */
        public string FromStationId { get; set; }
        /**
        *   ToStation Foreign Key
        */
        public string ToStationId { get; set; }
        /**
        *  Scooter'ın İstasyondan Cıkıs Saati.!-- Scooter Saat 2 de A Istasyondan Cıkıs Yaptı
        */
        public DateTime FromStationOutTime { get; set; }
        /**
        *  Scooter'ın Yeni istasyonuna Bırakılma Saati. Scooter Saat 4 te B istasyonuna bırakıldı
        */
        public DateTime ToStationInTime { get; set; }

        
       }
}