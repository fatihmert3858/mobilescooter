using EruMobileScooter.Data.Entities;

namespace MobileScooter.Api.Data.Entities
{
    public class Payment : BaseEntity {


        /**
        *   Hizmet Ucreti
        */
        public double Price { get; set; }
        /**
        * User Foreign Key
        */
        public string UserId { get; set; }
        /**
        *   Odemeyi Gerceklestiren User
        */
        public string ScooterTransportHistoryId { get; set; }
    }
}