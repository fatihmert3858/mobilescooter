using EruMobileScooter.Data.Entities;

namespace MobileScooter.Api.Data.Entities
{
    public class EnergyGenerator : BaseEntity {


        /**
        * Enerji Kapasitesi. 1 MW lık Bir Rüzgar Türbini
        */
        public int EnergyCapacity { get; set; }

        /**
        * Anlık Üretilen Enerji
        */
        public int CurrentEnergy { get; set; }
        
        /**
        * EnergyCreator Type. Rüzgar Tübini, Biyogaz, Günes Paneli
        */
        public string Type { get; set; }

        public string EnergyStationId { get; set; }
        public EnergyStation EnergyStation { get; set; }
    }

    public static class EnergyCreatorType {
        public const string NONE = "NONE";
        public const string WIND = "WIND";
        public const string SUN = "SUN";
        public const string WATER = "WATER";
        public const string BIOGAS = "BIOGAS";
    }
}