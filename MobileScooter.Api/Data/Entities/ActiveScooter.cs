using EruMobileScooter.Data.Entities;

namespace MobileScooter.Api.Data.Entities
{
    public class ActiveScooter : BaseEntity
    {
        public string ScooterId { get; set; }
        public string UserId { get; set; }
    }
}