using EruMobileScooter.Data.Entities;

namespace MobileScooter.Api.Data.Entities
{
    public class VerificationPassword : BaseEntity
    {
        public string UserEmail { get; set; }
        public string VerificationCode { get; set; }
    }
}