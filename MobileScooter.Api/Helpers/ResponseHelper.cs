using MobileScooter.Api.Models;

namespace MobileScooter.Api.Helpers
{
    public class ResponseHelper
    {
        public static ApiResponse<T> CreateApiResponse<T>(T data, string message, int statusCode, bool error){
            return new ApiResponse<T>{
                Data=data, Message = message, StatusCode = statusCode, Error = error
            };
        }

        public static SpecialResponse<T> CreateSpecialResponse<T>(T data,string succesMessage ,bool error,string errorName){
            return new SpecialResponse<T>{
                Data = data, Error=error, ErrorName = errorName, SuccessMessage =succesMessage
            };
        }
        public static SpecialResponse<T> CreateSpecialResponseFailled<T>(string errorName){
            return new SpecialResponse<T>{
                ErrorName = errorName, Error =true, SuccessMessage=""
            };
        }
        public static SpecialResponse<T> CreateSpecialResponseSucces<T>(T data,string succesMessage){
            return new SpecialResponse<T>{
                Data = data, SuccessMessage=succesMessage, Error=false, ErrorName = "", 
            };
        }
    }
}