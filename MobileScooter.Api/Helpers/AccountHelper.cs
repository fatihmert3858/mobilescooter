using System;
using MobileScooter.Api.Data.Entities;
using MobileScooter.Api.Models;

namespace MobileScooter.Api.Helpers
{
    public class AccountHelper
    {
    
        public static bool IsValidRegisterModel(RegisterModel model){
            if(model == null)
                return false;
            if(model.Email.Trim().Equals("") || model.Email == null)
                return false;
            else if(model.Password.Trim().Equals("") || model.Password == null)
                return false;
            else if(model.Name.Trim().Equals("") || model.Name == null)
                return false;
            else if(model.Identity.Trim().Equals("") || model.Identity == null)
                return false;
            else if(model.Faculty.Trim().Equals("") || model.Faculty == null)
                return false;
            else if(model.Gender == Gender.NONE)
                return false;
            else if(model.Role.Trim().Equals("") || model.Role == null)
                return false;
            else if(model.Department.Trim().Equals("") || model.Department == null)
                return false;
            return true;
        }

        public static bool IsValidLoginModel(LoginModel model)
        {
            if(model == null)
                return false;
            else if(model.Password.Equals("") || model.Password == null)
                return false;
            else if(model.Email.Equals("") || model.Email == null)
                return false;

            return true;
        }

        public static bool IsValidRefreshPasswordModel(RefreshPasswordModel model)
        {
            if(model == null)
                return false;
            else if(model.CurrentPassword.Equals("") || model.CurrentPassword == null)
                return false;
            else if(model.NewPassword.Equals("") || model.NewPassword == null)
                return false;
            else if(model.Email.Equals("") || model.Email == null)
                return false;
            return true;
        }


        public static bool IsValidResetPasswordModel(ResetPasswordModel model){
            if(model == null)
                return false;
            else if(model.Email.Equals("") || model.Email == null)
                return false;
            else if(model.NewPassword.Equals("") || model.NewPassword == null)
                return false;
            else if(model.Identity.Equals("") || model.Identity == null)
                return false;
            return true;
        }

        public static bool IsValidForgotPasswordModel(ForgotPasswordModel model){
            if(model == null)
                return false;
            else if(model.Email.Equals("") || model.Email == null)
                return false;
            else if(model.Name.Equals("") || model.Name == null)
                return false;
            else if(model.Identity.Equals("") || model.Identity == null)
                return false;
                
            return true;
        }
    }
}